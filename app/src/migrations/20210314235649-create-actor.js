'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Actors', {
      id: {
        allowNull     : false,
        autoIncrement : true,
        primaryKey    : true,
        type          : Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      movie_id: {
        type       : Sequelize.INTEGER,
        references : { model: 'Movies', key: 'id' },
        onUpdate   : 'CASCADE',
        onDelete   : 'SET NULL',
        allowNull  : true
      },
      createdAt: {
        allowNull : false,
        type      : Sequelize.DATE
      },
      updatedAt: {
        allowNull : false,
        type      : Sequelize.DATE
      }
    });
  },
  // eslint-disable-next-line no-unused-vars
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Actors');
  }
};