'use strict';

//
// dependencies
const jwt = require('jsonwebtoken');
const { error } = require('common-api');
const i18n = require('../i18n');

//
// exports
const verify = async (req, res, next) => {
  const authorization = req.headers['x-access-token'];

  if (authorization) {
    try {
      req.authorizer = jwt.decode(authorization, process.env.ACCESS_TOKEN_SECRET, {complete: true});
      return next();
    } catch (err) {
      if (err.error) {
        throw new error.HttpError(err.error.message, err.status_code, err.error.code);
      }
      throw new error.HttpError('Error on validate token.', 401, 'backend-403_error-validate-token');
    }
  } else {
    res.status(500).json({ message: 'Login inválido!' });
  }
  throw new error.HttpError('Missing autentication token.', 403, 'backend-403_missing-authentication-token');
};

const validateRole = async (req, res, next) => {
  if (req.authorizer.user.role !== 'admin') {
    throw new error.HttpError(
      i18n.__('notAllowUser'),
      403,
      'backend-403_error-getAll-user'
    );
  } else {
    return next();
  }
};

module.exports = {
  verify,
  validateRole
};