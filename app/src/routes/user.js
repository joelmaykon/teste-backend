
'use strict';

//
// middlewares
const authorizerMiddleware = require('../middlewares/authorizer.middleware');

//
// users
const usersRetrieveController = require('../controllers/users/retrieve.controller');
const usersPersistController = require('../controllers/users/persist.controller');

const init = (expressInstance, basePath) => {
  expressInstance.get(`${basePath}/users/:id`, authorizerMiddleware.verify, usersRetrieveController.getById);
  expressInstance.get(`${basePath}/users`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, usersRetrieveController.getAll);
  expressInstance.post(`${basePath}/users`, authorizerMiddleware.verify, usersPersistController.create);
  expressInstance.put(`${basePath}/users/:id`, authorizerMiddleware.verify, usersPersistController.edit);
  expressInstance.delete(`${basePath}/users/:id`, authorizerMiddleware.verify, usersPersistController.remove);
};

module.exports = {
  init
};