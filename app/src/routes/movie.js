
'use strict';

//
// middlewares
const authorizerMiddleware = require('../middlewares/authorizer.middleware');

//
// movies
const movieRetrieveController = require('../controllers/movies/retrieve.controller');
const moviePersistController = require('../controllers/movies/persist.controller');

const init = (expressInstance, basePath) => {
  expressInstance.get(`${basePath}/movies/:id`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, movieRetrieveController.getById);
  expressInstance.get(`${basePath}/movies`, movieRetrieveController.getAll);
  expressInstance.post(`${basePath}/movies`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, moviePersistController.create);
  expressInstance.put(`${basePath}/movies/:id`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, moviePersistController.edit);
  expressInstance.delete(`${basePath}/movies/:id`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, moviePersistController.remove);
};

module.exports = {
  init
};