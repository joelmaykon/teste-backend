
'use strict';

//
// middlewares
const authorizerMiddleware = require('../middlewares/authorizer.middleware');

//
// admin
const movieRetrieveController = require('../controllers/admins/retrieve.controller');
const moviePersistController = require('../controllers/admins/persist.controller');

const init = (expressInstance, basePath) => {
  expressInstance.get(`${basePath}/admins/:id`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, movieRetrieveController.getById);
  expressInstance.get(`${basePath}/admins`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, movieRetrieveController.getAll);
  expressInstance.post(`${basePath}/admins`, /* authorizerMiddleware.verify, authorizerMiddleware.validateRole,  */moviePersistController.create);
  expressInstance.put(`${basePath}/admins/:id`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, moviePersistController.edit);
  expressInstance.delete(`${basePath}/admins/:id`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, moviePersistController.remove);
};

module.exports = {
  init
};