'use strict';

//
// routes
const votes = require('./vote.js');
const movie = require('./movie.js');
const admin = require('./admin.js');
const user = require('./user.js');
const auth = require('./auth.js');

//
// init routes
module.exports.init = (expressInstance, basePath) => {
  votes.init(expressInstance, basePath);
  movie.init(expressInstance, basePath);
  admin.init(expressInstance, basePath);
  user.init(expressInstance, basePath);
  auth.init(expressInstance, basePath);
};