
'use strict';

//
// auth
const authRetrieveController = require('../controllers/auth/retrieve.controller');

const init = (expressInstance, basePath) => {
  expressInstance.post(`${basePath}/authentication`, authRetrieveController.login);
};

module.exports = {
  init
};