
'use strict';

//
// middlewares
const authorizerMiddleware = require('../middlewares/authorizer.middleware');

//
// votes
const moviePersistController = require('../controllers/votes/persist.controller');

const init = (expressInstance, basePath) => {
  expressInstance.post(`${basePath}/votes`, authorizerMiddleware.verify, authorizerMiddleware.validateRole, moviePersistController.create);
};

module.exports = {
  init
};