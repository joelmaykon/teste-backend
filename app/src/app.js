'use strict';

//
// common api
const { express } = require('common-api');
require('dotenv').config();
const i18n = require('./i18n');
const errorMiddleware = require('./middlewares/error.middleware');
const timezoneMiddleware = require('./middlewares/timezone.middleware');
const logger = require('morgan');
const helmet = require('helmet');


//
// initialize i18n
express.instance.use(i18n.init);

//
// initialize middleware
express.instance.use(errorMiddleware);
express.instance.use(timezoneMiddleware);
express.instance.use(logger('dev'));
express.instance.use(helmet());
express.instance.disable('x-powered-by');

const serviceDB = require('./models');
const startDB = async () => {
  return await serviceDB.sequelize.sync();
};

startDB();

//
// health check to target group
express.instance.get('/healthcheck', async (req, res) => {
  res.status(200).send({ message: i18n.__('systemOnline'), build: process.env.BUILD_NUMBER || null, dbIsActive: await startDB() ? true : false });
});

//
// routes
const basePath = '/backend/v1';
const routes = require('./routes/index.js');
routes.init(express.instance, basePath);
//
// route health check application
express.instance.get(`${basePath}/healthcheck`, async (req, res) => {
  res.status(200).send({ message: 'ok', dbIsActive: await startDB() ? true : false });
});
const port = process.env.PORT || 5000;
express.instance.listen(port, async () => {
  // eslint-disable-next-line no-console
  console.log(`Server is listening on port ${port}`);
});
//
// express 

express.init();
module.exports = express.instance;