const i18n = require('../../i18n');
const { validator, response, error } = require('common-api');
const models = require('../../models');
const { User } = models;

const _validateCreateBody = (body) => {
  const createSchema = {
    id         : '/CreateUser',
    type       : 'object',
    properties : {
      userName : { type: 'string' },
      name     : { type: 'string' },
      password : { type: 'string' }
    },
    required: ['userName', 'name', 'password']
  };
  return validator.validate(createSchema, body);
};

const _validateEditBody = (body) => {
  const editSchema = {
    id         : '/EditUser',
    type       : 'object',
    properties : {
      userName : { type: 'string' },
      name     : { type: 'string' },
      password : { type: 'string' }
    }
  };
  return validator.validate(editSchema, body);
};

exports.create = async (req, res) => {
  const { userName, name, password } = _validateCreateBody(req.body);
  const user = { userName, name, password, role: 'admin', is_deleted: false };
  const UserExist = await User.findOne({
    where: { userName: user.userName }
  });
  try {
   
    if (!UserExist) {
      User.create(user)
        .then((user) => {
          return response.success(res, user, 201);
        })
        .catch((err) => {
          if (err.name === 'HttpError') {
            throw err;
          }
          throw new error.HttpError(
            i18n.__('createdError', i18n.__('user')),
            400,
            'backend-400_error-create-user'
          );
        });
    } else {
      throw new error.HttpError(
        i18n.__('userNameExist', i18n.__('user')),
        400,
        'backend-400_error-create-username-existing'
      );
    }
  } catch (err) {
    if (err.name === 'HttpError') {
      throw err;
    }
    throw new error.HttpError(
      i18n.__('createdError', i18n.__('user')),
      500,
      'backend-500_error-create-user'
    );
  }
};

exports.edit = (req, res) => {
  const id = req.params.id;
  User.update(_validateEditBody(req.body), {
    where: { id: id }
  })
    .then((num) => {
      if (num) {
        return response.success(res,
          i18n.__('successUpdate'),
          200);
      }
    })
    .catch(() => {
      throw new error.HttpError(
        i18n.__('errorUpdated'),
        500,
        'backend-500_error-update-user'
      );
    });
};

exports.remove = async (req, res) => {
  const id = req.params.id;
  const { user } = req.authorizer;
  if (user.id !== parseInt(id)) {
    User.update({ is_deleted: true }, {
      where: { id: id }
    })
      .then(() => {
        return response.success(res,
          {},
          204);
      }).catch(() => {
        throw new error.HttpError(
          i18n.__('errorRemove'),
          400,
          'backend-400_error-create-username-existing'
        );
      });
  } else {
    throw new error.HttpError(
      i18n.__('errorEqualUserId'),
      400,
      'backend-400_error-create-username-existing'
    );
  }
};

