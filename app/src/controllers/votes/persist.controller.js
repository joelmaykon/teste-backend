const i18n = require('../../i18n');
const { validator, response, error } = require('common-api');
const models = require('../../models');
const { Vote, Movie } = models;

const _validateCreateBody = (body) => {
  const createSchema = {
    id         : '/CreateVote',
    type       : 'object',
    properties : {
      vote    : { type: 'int' },
      movieId : { type: 'int' }
    },
    required: ['vote', 'movieId']
  };
  return validator.validate(createSchema, body);
};


exports.create = async (req, res) => {
  const { vote, movieId } = _validateCreateBody(req.body);
  const _vote = { vote, movie_id: movieId };
  const movieExist = await Movie.findOne({
    where: { id: _vote.movie_id }
  });
  if (!movieExist) {
    throw new error.HttpError(
      i18n.__('movieIdNotExist', _vote.movie_id),
      400,
      'backend-400_error-create-vote'
    );
  }
  try {
    Vote.create(_vote)
      .then((vote) => {
        return response.success(res, vote, 201);
      })
      .catch((err) => {
        if (err.name === 'HttpError') {
          throw err;
        }
        throw new error.HttpError(
          i18n.__('createdError', i18n.__('vote')),
          400,
          'backend-400_error-create-vote'
        );
      });
  } catch (err) {
    if (err.name === 'HttpError') {
      throw err;
    }
    throw new error.HttpError(
      i18n.__('createdError', i18n.__('vote')),
      500,
      'backend-500_error-create-vote'
    );
  }
};

