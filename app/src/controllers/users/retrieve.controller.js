const i18n = require('../../i18n');
const { response, error } = require('common-api');
const models = require('../../models');
const { User } = models;

exports.getById = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then((user) => {
      return response.success(res, user, 201);
    })
    .catch((err) => {
      if (err.name === 'HttpError') {
        throw err;
      }

      throw new error.HttpError(
        i18n.__('notFound', i18n.__('user'), id),
        500,
        'backend-500_error-getById-user'
      );
    });
};

exports.getAll = (req, res) => {  
  User.findAll({ where: { role: 'user', is_deleted: false } })
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((err) => {
      if (err.name === 'HttpError') {
        throw err;
      }
      throw new error.HttpError(
        err.message || i18n.__('notFound'),
        500,
        'backend-500_error-getAll-user'
      );
    });
};
