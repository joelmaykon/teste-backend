const i18n = require('../../i18n');
const { error, response } = require('common-api');
require('dotenv').config();
const jwt = require('jsonwebtoken');
const models = require('../../models');
const { User } = models;

exports.login = async (req, res) => {
  const { username, password } = req.body;
  try {
    const user = await User.findOne({
      where: { userName: username }
    });
    const autorizer = user.correctPassword(password);
    // Neither do this!
    if (!autorizer) {
      throw new error.HttpError(
        i18n.__('notAuthorized', i18n.__('user'), username),
        401,
        'backend-401_error-getByUserName-user'
      );
    }

    //create the access token with the shorter lifespan   
    const accessToken = jwt.sign({ user }, `${process.env.ACCESS_TOKEN_SECRET}`, {
      algorithm : 'HS256',
      expiresIn : `${process.env.ACCESS_TOKEN_LIFE}h`
    });

    //create the refresh token with the longer lifespan
    const refreshToken = jwt.sign({ user }, `${process.env.REFRESH_TOKEN_SECRET}`, {
      algorithm : 'HS256',
      expiresIn : `${process.env.REFRESH_TOKEN_LIFE}h`
    });

    //store the refresh token in the user array
    user.refreshToken = refreshToken;
    return response.success(res, { bearer: accessToken }, 201);

  } catch (err) {
    if (err.name === 'HttpError') {
      throw err;
    }

    throw new error.HttpError(
      i18n.__('notFound', i18n.__('user'), username),
      500,
      'backend-500_error-getByUserName-user'
    );
  }
};

