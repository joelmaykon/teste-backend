const i18n = require('../../i18n');
const { validator, response, error } = require('common-api');
const models = require('../../models');
const { Movie, Actor, Vote } = models;
const { Op } = require('sequelize');

exports.getById = (req, res) => {
  const id = req.params.id;

  Movie.findByPk(id)
    .then((movie) => {
      return response.success(res, movie, 201);
    })
    .catch((err) => {
      if (err.name === 'HttpError') {
        throw err;
      }

      throw new error.HttpError(
        i18n.__('notFound', i18n.__('user'), id),
        500,
        'backend-500_error-getById-user'
      );
    });
};

const _validateBody = (body) => {
  const createSchema = {
    id         : '/getAllMovies',
    type       : 'object',
    properties : {
      name     : { type: 'string' },
      genre    : { type: 'string' },
      director : { type: 'string' }
    }
  };
  return validator.validate(createSchema, body);
};

exports.getAll = (req, res) => {
  const { name, genre, director, actor } = _validateBody(req.query);
  const where = {};
  if (name) {
    where.name = {
      [Op.like]: `%${name}%`
    };
  }

  if (genre) {
    where.genre = {
      [Op.like]: `%${genre}%`
    };
  }

  if (director) {
    where.director = {
      [Op.like]: `%${director}%`
    };
  }

  const whereActor = {};
  if (actor) {
    whereActor.name = {
      [Op.like]: `%${actor}%`
    };
  }

  Movie.findAll({
    where,
    include: [{
      model : Actor,
      as    : 'actors',
      where : whereActor
    }, {
      model : Vote,
      as    : 'votes'
    }]
  })
    .then(async (listMovies) => {
      res.status(200).send(listMovies);
    })
    .catch((err) => {
      if (err.name === 'HttpError') {
        throw err;
      }
      throw new error.HttpError(
        err.message || i18n.__('notFound'),
        500,
        'backend-500_error-getAll-user'
      );
    });
};
