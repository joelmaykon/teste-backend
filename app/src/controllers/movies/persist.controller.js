const i18n = require('../../i18n');
const { validator, response, error } = require('common-api');
const models = require('../../models');
const { Movie, Actor } = models;

const _validateCreateBody = (body) => {
  const createSchema = {
    id         : '/CreateMovie',
    type       : 'object',
    properties : {
      name     : { type: 'string' },
      genre    : { type: 'string' },
      director : { type: 'string' },
      actors   : { type: 'array' }
    },
    required: ['name', 'genre', 'director', 'actors']
  };
  return validator.validate(createSchema, body);
};

const _validateEditBody = (body) => {
  const editSchema = {
    id         : '/EditMovie',
    type       : 'object',
    properties : {
      name     : { type: 'string' },
      genre    : { type: 'string' },
      director : { type: 'string' }
    }
  };
  return validator.validate(editSchema, body);
};

exports.create = async (req, res) => {
  const { name, genre, director, actors } = _validateCreateBody(req.body);
  const movie = { name, genre, director, actors };
  if (movie.actors.length === 0) {
    throw new error.HttpError(
      i18n.__('actorsEmpty', i18n.__('movie')),
      400,
      'backend-400_error-create-movie'
    );
  }
  try {
    const MovieExist = await Movie.findOne({
      where: { name: movie.name }
    });
    if (!MovieExist) {
      Movie.create(movie,
        {
          include: [{
            model : Actor,
            as    : 'actors'
          }]
        })
        .then((movie) => {
          return response.success(res, movie, 201);
        })
        .catch((err) => {
          if (err.name === 'HttpError') {
            throw err;
          }
          throw new error.HttpError(
            i18n.__('createdError', i18n.__('user')),
            400,
            'backend-400_error-create-user'
          );
        });
    } else {
      throw new error.HttpError(
        i18n.__('userNameExist', i18n.__('movie')),
        400,
        'backend-400_error-create-username-existing'
      );
    }
  } catch (err) {
    if (err.name === 'HttpError') {
      throw err;
    }
    throw new error.HttpError(
      i18n.__('createdError', i18n.__('user')),
      500,
      'backend-500_error-create-user'
    );
  }
};

exports.edit = (req, res) => {
  const id = req.params.id;
  Movie.update(_validateEditBody(req.body), {
    where: { id: id }
  })
    .then((num) => {
      if (num) {
        return response.success(res,
          i18n.__('successUpdate'),
          200);
      }
    })
    .catch(() => {
      throw new error.HttpError(
        i18n.__('errorUpdated'),
        500,
        'backend-500_error-update-user'
      );
    });
};

exports.remove = async (req, res) => {
  const id = req.params.id;
  const { movie } = req.authorizer;
  if (movie.id !== parseInt(id)) {
    Movie.update({ is_deleted: true }, {
      where: { id: id }
    })
      .then(() => {
        return response.success(res,
          {},
          204);
      }).catch(() => {
        throw new error.HttpError(
          i18n.__('errorRemove'),
          400,
          'backend-400_error-create-username-existing'
        );
      });
  } else {
    throw new error.HttpError(
      i18n.__('errorEqualUserId'),
      400,
      'backend-400_error-create-username-existing'
    );
  }
};

