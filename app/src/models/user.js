'use strict';
/**
 * Using Sequelize Hooks and Crypto to Encrypt User Passwords
 */
const bcrypt = require('bcrypt');

const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here     
    }
  }
  User.init({
    userName : DataTypes.STRING,
    name     : DataTypes.STRING,
    password : {
      type: DataTypes.STRING,
      get() {
        return () => this.getDataValue('password');
      }
    },
    salt: {
      type: DataTypes.STRING,
      get() {
        return () => this.getDataValue('salt');
      }
    },
    role       : DataTypes.STRING,
    is_deleted : DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'User'
  });
  User.generateSalt = function () {
    return bcrypt.genSaltSync(10);
  };
  User.encryptPassword = function (password, salt) {
    return bcrypt.hashSync(password, salt);
  };
  const setSaltAndPassword = user => {
    if (user.changed('password')) {
      user.salt = User.generateSalt();
      user.password = User.encryptPassword(user.password(), user.salt());
    }
  };
  User.beforeCreate(setSaltAndPassword);
  User.beforeUpdate(setSaltAndPassword);
  // verify if password is correct
  User.prototype.correctPassword = function (enteredPassword) {
    return User.encryptPassword(enteredPassword, this.salt()) === this.password();
  };
  return User;
};