'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
      Movie.hasMany(models.Actor, {
        as         : 'actors',
        foreignKey : 'movie_id',
        onDelete   : 'CASCADE',
        onUpdate   : 'CASCADE'
      }); 
      Movie.hasMany(models.Vote, {
        as         : 'votes',
        foreignKey : 'movie_id',
        onDelete   : 'CASCADE',
        onUpdate   : 'CASCADE'
      }); 
           
    }
  }
  Movie.init({
    name     : DataTypes.STRING,
    genre    : DataTypes.STRING,
    director : DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Movie'
  });

 
  return Movie;
};