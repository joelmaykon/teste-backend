'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vote extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    // eslint-disable-next-line no-unused-vars
    static associate(models) {
      // define association here
    }
  }
  Vote.init({
    vote     : DataTypes.INTEGER,
    movie_id : DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Vote'
  });
  return Vote;
};